<!doctype html>
<!-- If multi-language site, reconsider usage of html lang declaration here. -->
<html lang="en"> 
<head>
	<meta charset="utf-8">
	<!-- Setting the viewport for Media Query -->
	<meta name="viewport" content="width=device-width,initial-scale=1.0">
	<title>Framework</title>
	<!-- Place favicon.ico in the root directory: mathiasbynens.be/notes/touch-icons -->
	<link rel="shortcut icon" href="favicon.ico" />	
	<!-- Adding reference to font awesome -->
	<link rel="stylesheet" href="assets/vendor/font/fontawesome-all.css">
	<!-- Default style-sheet is for 'media' type screen (color computer display).  -->
	<link rel="stylesheet" media="screen" href="assets/css/style.css">
</head>
<body>
	<div class="container">
		<!-- Start of the header -->
		<header>			
		</header>
		<!-- end of header -->
		<!-- Start of main -->
		<main>
		<form action="assets/include/login.inc.php" method="post">
		<input type="text" name="username" placeholder="please enter your name">
		<input type="password" name="password" placeholder="please enter your password">
		<input type="submit" name="submit" value="submit">
		<a href="assets/php/register.php">Register now</a>
		</form>
		<a href="assets/php/admin.php">I am admin</a>
		</main>
		<!-- End of main -->
		<!-- start of footer -->
		<footer>			
		</footer>
		<!-- end of footer -->
	</div>
	<script src="assets/js/script.js"></script>
</body>
</html>