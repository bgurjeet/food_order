<?php 
@include '../classes/product.php';


if(isset($_POST['submit'])) {
    $data = $_FILES['img'];
    $name = $_FILES['img']['name'];
    $path = $_FILES['img']['tmp_name'];
    $newpath = "../images/".$name;
    move_uploaded_file($path,$newpath);

    $dishname = $_POST['name'];
    $dishimg = $newpath;
    $dishprice = $_POST['price'];
    
    if($dishes->addProduct($dishname, $dishimg, $dishprice)) {
        header("Location:../php/addItem.php");
    }

}

?>