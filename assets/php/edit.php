<html>
<head>
<title>upadate or delete menu</title>
<link rel="stylesheet" media="screen" href="../css/style.css">
<head>
<html>
<body>
    <div class="container">
    <table>
    <?php 
    
    $productID;
    @include '../classes/product.php';
    $products = $dishes->getProduct();
    
    function vals()
    {
    global $products;
    
    foreach($products as $values) {
        ?>
        <tr>
            <td><?php echo $values['dish_name'] ;?></td>
            <td><figure><img src=" <?php echo $values['dish_img']; ?> "></figure></td>
            <td><span><?php echo $values['dish_price'] ?></span></td>
            <td><a href="edit.php?edit=<?php echo $values['ID']?>">Edit</a> | <a href="edit.php?delete=<?php echo $values['ID']?>">Delete</a></td></tr>
        </tr>
        <?php 
    }
}  

vals();
    
    if(isset($_GET['edit'])) {
        global $productID;
        $productID = $_GET['edit'];
        $upproducts = $dishes->updateProduct($productID);
    }

    if(isset($_GET['delete'])) {
        $deletedID = $_GET['delete'];
        $dishes->delete($deletedID);
        header("Location:edit.php");
    }

    if(isset($_POST['update'])) {
        $dishprice = $_POST['price'];
        $idval = $_GET['edit'];
        $dishname = $_POST['updatename'];

        if(isset( $_FILES["img"] ) && !empty( $_FILES["img"]["name"] )) {
            $data = $_FILES['img'];
            $name = $_FILES['img']['name'];
            $path = $_FILES['img']['tmp_name'];
            $newpath = "../images/".$name;
            move_uploaded_file($path,$newpath);
            
            $dishimg = $newpath;
            $dishes->saveUpdate($idval,$dishname, $dishimg, $dishprice);
        } else {
            $dishes->secUpdate($idval,$dishname, $dishprice);
        }

        header("Location:edit.php");
    }


    ?>
    </table>


    <form action="edit.php?edit=<?php echo $_GET['edit'];?>" method="post" enctype="multipart/form-data">
    <input type="text" name="updatename" value="<?php echo $upproducts['dish_name'] ?>">
    <br>
    <input type="file" name="img">
    <br>
    <input type="number" name="price" value="<?php echo $upproducts['dish_price'] ?>">
    <br>
    <input type="submit" name="update" value="update">
    
    </form>
    </div>
</body>
</html>
</html>